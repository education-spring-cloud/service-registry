# Eureka service registry

## Docker 
To build image from this directory:
```
docker build --tag gopas/service-registry .
```
And run it:
```
docker run -d --rm --name eureka01 -p8761:8761 --hostname eureka01 gopas/service-registry
```
Stop it:
```
docker stop registry 
```
Remove container:
```
docker rm registry
```